﻿using System;
using System.IO;
using System.Text;

namespace ConsoleApp456
{
    class Program
    {
        const string PATH_TO_FILE = "Notebook.txt";

        static void Main(string[] args)
        {
            Menu();
        }
        static void Menu()
        {
            ConsoleKeyInfo consoleKeyInfo;
            do
            {
                Console.WriteLine("Введите значение из указанного функционала.\n" +
                    "1) Пример заполнения документа.\n" +
                    "2) Заполнение данных и добавление их в новую запись в конец файла.\n" +
                    "3) Вывод данных на экран.\n" +
                    "4) Закончить выполнение программы.");
                consoleKeyInfo = Console.ReadKey();
                Console.WriteLine();

                switch (consoleKeyInfo.KeyChar)
                {
                    case '1':
                        PrintExampleDocument();
                        break;
                    case '2':
                        Input();
                        break;
                    case '3':
                        Print();
                        break;
                    case '4':
                        break;
                    default:
                        Console.WriteLine("Неизвестный пункт меню");
                        break;
                }
            }
            while (consoleKeyInfo.Key != ConsoleKey.D4);
        }
        static void PrintExampleDocument()
        {
            Console.WriteLine("1#20.12.2021 00:12#Иванов Иван Иванович#25#176#05.05.1992#город Москва\n" +
                              "2#15.12.2021 03:12#Алексеев Алексей Иванович#24#176#05.11.1980#город Томск");
        }

        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        static bool StringIsDigits(string str)
        {
            foreach (var item in str)
            {
                if (!char.IsDigit(item))
                    return false;
            }
            return true;
            
        }
        static void Input()
        {
            char splitter = '#';
            StringBuilder stringBuilder = new StringBuilder();
            int id = 1;
            if (!File.Exists(PATH_TO_FILE))
            {
                File.Create(PATH_TO_FILE).Close();
                Console.WriteLine("Файл создан");
            }
            else
            {
                id = File.ReadAllLines(PATH_TO_FILE).Length + 1;
            }
            Console.WriteLine($"Id {id}: Дата и время добавления записи: { DateTime.Now.ToString() }");
            stringBuilder.Append($"{id++}{splitter}");
            stringBuilder.Append($"{DateTime.Now.ToString()}{splitter}");
            Console.WriteLine("\nВведите Ф.И.О: ");
            string fullName = Console.ReadLine();
            while (IsDigitsOnly(fullName) == true)
            {
                Console.WriteLine("Вы ввели некорректные данные, введите имя еще раз");
                fullName = Console.ReadLine();
            }
            stringBuilder.Append($"{fullName}{splitter}");

            Console.WriteLine("Введите возраст: ");
            string age = Console.ReadLine();
            while (StringIsDigits(age) == false)
            {
                Console.WriteLine("Вы ввели некорректные данные, введите возраст в виде целочисленного значения");
                age = Console.ReadLine();
            }
            stringBuilder.Append($"{age}{splitter}");

            Console.WriteLine("Введите рост: ");
            string height = Console.ReadLine();
            while (StringIsDigits(height) == false)
            {
                Console.WriteLine("Вы ввели некорректные данные, введите рост в виде целочисленного значения");
                height = Console.ReadLine();
            }
            stringBuilder.Append($"{height}{splitter}");

            Console.WriteLine("Введите дату рождения: ");
            string dataOfBirth = DateTime.Parse(Console.ReadLine()).ToShortDateString();
            stringBuilder.Append($"{dataOfBirth}{splitter}");

            Console.WriteLine("Введите место рождения: ");
            string placeOfBirth = Console.ReadLine();
            while (IsDigitsOnly(placeOfBirth) == true)
            {
                Console.WriteLine("Вы ввели некорректные данные, введите город в котором вы родились еще раз");
                placeOfBirth = Console.ReadLine();
            }
            stringBuilder.Append($"{placeOfBirth}");
            using (StreamWriter list = new StreamWriter("Notebook.txt", true, Encoding.Unicode))
            {
                list.WriteLine(stringBuilder.ToString());
            }
        }
        static void Print()
        {
            if (!File.Exists(PATH_TO_FILE))
            {
                Console.WriteLine("Файл не существует");
                return;
            }
            using (StreamReader list2 = new StreamReader(PATH_TO_FILE, Encoding.Unicode))
            {
                string line;
                Console.WriteLine($"{"Id",5}{"Дата и время",20}{"Ф.И.О",15} {"Возраст",15} {"Рост",15} {"Дата Рождения",15} {"Место",20}");
                while ((line = list2.ReadLine()) != null)
                {
                    string[] date = line.Split('#');
                    Console.WriteLine($"{date[0],5}{date[1],20} {date[2],14} {date[3],15} {date[4],15} {date[5],15} {date[6],20}");
                }
            }
        }
    }
}